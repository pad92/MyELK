#!/bin/sh


curl -XPUT 'http://localhost:9200/_template/padselk-esxi' -d@/etc/logstash/template/padselk-esxi.json
curl -XPUT 'http://localhost:9200/_template/padselk-firewall' -d@/etc/logstash/template/padselk-firewall.json
curl -XPUT 'http://localhost:9200/_template/padselk-syslog' -d@/etc/logstash/template/padselk-syslog.json
curl -XPUT 'http://localhost:9200/_template/padselk-http' -d@/etc/logstash/template/padselk-http.json

