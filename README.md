# Configuration ELK

- entrée
 - syslog
- filtres
 - pfsense 2.3.2
 - nginx ( vhosts combined )

## Version d'ELK utilisé
- logstash 5.x.x
- elasticsearch 5.x.x
- kibana 5.x.x

## Depots debian

```
deb https://artifacts.elastic.co/packages/5.x/apt stable main
```

# Templates Elastic

```
curl -XPUT 'http://localhost:9200/_template/padselk-esxi'     -d@/etc/logstash/template/padselk-esxi.json
curl -XPUT 'http://localhost:9200/_template/padselk-firewall' -d@/etc/logstash/template/padselk-firewall.json
curl -XPUT 'http://localhost:9200/_template/padselk-syslog'   -d@/etc/logstash/template/padselk-syslog.json
curl -XPUT 'http://localhost:9200/_template/padselk-http'     -d@/etc/logstash/template/padselk-http.json
```


# Configuration spécifique
## Nginx

```
log_format combined_vhost '$host $remote_addr - $remote_user [$time_local] '
    '"$request" $status $body_bytes_sent '
    '"$http_referer" "$http_user_agent" $request_time $upstream_response_time';

access_log syslog:server=127.0.0.1:5140 combined_vhost;
error_log  syslog:server=127.0.0.1:5140 warn;
```

# Sources

* https://elijahpaul.co.uk/updated-monitoring-pfsense-logs-using-elk-elasticsearch-logstash-kibana-part-1/
* https://elijahpaul.co.uk/monitoring-pfsense-2-1-logs-using-elk-logstash-kibana-elasticsearch/
